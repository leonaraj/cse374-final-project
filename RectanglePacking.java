import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Arrays;
import java.util.Comparator;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class RectanglePacking {

	public static void main(String[] args) {
		int[] container = {700, 900};
		int[][] rects = {{273, 395}, {318, 99}, {259, 476}, {375, 547}, {207, 248}, {58, 276}, {92, 381}, {168, 100}};
		long startTime = System.currentTimeMillis();
		Packing p = packRectangles(container, rects);
		long elapsedTime = System.currentTimeMillis() - startTime;
		if (p == null) {
			System.out.println("NO PACKING FOUND");
		} else {
			displayGraphics(p);
		}
		System.out.println("Time taken: " + elapsedTime + " ms");
	}
	
	public static Packing packRectangles(int[] container, int[][] rects) {
		
		// sorts by decreasing area
		Arrays.<int[]>sort(rects, new Comparator<int[]>() {
			@Override
			public int compare(int[] rect1, int[] rect2) {
				int area1 = rect1[0] * rect1[1];
				int area2 = rect2[0] * rect2[1];
				return area2 - area1;
			}
		});
		
		Packing packing = new Packing(container, rects);
		for (int i = 0; i < rects.length; i++) {
			Rectangle rect = new Rectangle(rects[i]);
			for (int theta = 0; theta < 2; theta++) {
				rect.rotate(theta);
				if (packing.canBeAdded(rect)) {
					packing.rects[i] = rect;
					completePacking(packing, rects);
					if (packing.isComplete()) return packing;
					packing.rects[i] = null;
				}
			}
		}
		return null;
	}
	
	private static void completePacking(Packing packing, int[][] rects) {
		if (packing.isComplete()) return;
		for (int i = 0; i < rects.length; i++) {
			if (packing.rects[i] != null) continue;
			Rectangle rect = new Rectangle(rects[i]);
			for (int theta = 0; theta < 2; theta++) {
				rect.rotate(theta);
				for (Rectangle r1 : packing.rects) {
					if (r1 == null) continue;
					
					rect.setPos(0, r1.topY());
					if (packing.canBeAdded(rect) && packing.canBeWedgedBetweenTopOfRectangleAndLeftWall(rect, r1)) {
						packing.rects[i] = rect;
						completePacking(packing, rects);
						if (packing.isComplete()) return;
						packing.rects[i] = null;
					}
					
					rect.setPos(r1.rightX(), 0);
					if (packing.canBeAdded(rect) && packing.canBeWedgedBetweenRightOfRectangleAndFloor(rect, r1)) {
						packing.rects[i] = rect;
						completePacking(packing, rects);
						if (packing.isComplete()) return;
						packing.rects[i] = null;
					}
					
					for (Rectangle r2 : packing.rects) {
						if (r2 == null) continue;
						rect.setPos(r1.rightX(), r2.topY());
						if (packing.canBeAdded(rect) && packing.canBeWedgedBetweenTopOfRectangleAndRightOfRectangle(rect, r2, r1)) {
							packing.rects[i] = rect;
							completePacking(packing, rects);
							if (packing.isComplete()) return;
							packing.rects[i] = null;
						}
					}
				}
			}
		}
	}
	
	public static class Packing {
		
		Rectangle container;
		Rectangle[] rects;
		
		private Packing(int[] container, int[][] rects) {
			this.container = new Rectangle(container);
			this.rects = new Rectangle[rects.length];
		}
		
		private boolean isComplete() {
			for (Rectangle rect : rects) {
				if (rect == null) return false;
			}
			return true;
		}
		
		private boolean canBeAdded(Rectangle rect) {
			return (fitsInContainer(rect) && !intersectsOtherRects(rect));
		}
		
		private boolean fitsInContainer(Rectangle rect) {
			return (rect.x >= 0 && rect.y >= 0 && rect.rightX() <= container.width && rect.topY() <= container.height);
		}
		
		private boolean intersectsOtherRects(Rectangle rect) {
			for (Rectangle r : rects) {
				if (r != null && rect.intersects(r)) return true;
			}
			return false;
		}
		
		private boolean canBeWedgedBetweenTopOfRectangleAndLeftWall(Rectangle rect, Rectangle topRect) {
			return (topRect.x < rect.width);
		}
		
		private boolean canBeWedgedBetweenRightOfRectangleAndFloor(Rectangle rect, Rectangle rightRect) {
			return (rightRect.y < rect.height);
		}
		
		private boolean canBeWedgedBetweenTopOfRectangleAndRightOfRectangle(Rectangle rect, Rectangle topRect, Rectangle rightRect) {
			int xDist = topRect.x - rightRect.rightX();
			int yDist = rightRect.y - topRect.topY();
			return (xDist > -topRect.width && xDist < rect.width && yDist > -rightRect.height && yDist < rect.height);
		}
	}
	
	private static class Rectangle {
		
		private int x; // x-coordinate of lower-left corner
		private int y; // y-coordinate of lower-left corner
		private int width;
		private int height;
		
		private Rectangle(int[] dimensions) {
			this(dimensions[0], dimensions[1]);
		}
		
		private Rectangle(int width, int height) {
			this(0, 0, width, height);
		}
		
		private Rectangle(int x, int y, int width, int height) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}
		
		private void rotate(int theta) {
			if (theta == 1) {
				int temp = width;
				width = height;
				height = temp;
			}
		}
		
		private int rightX() {
			return x + width;
		}
		
		private int topY() {
			return y + height;
		}
		
		private void setPos(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		private boolean intersects(Rectangle rect) {
			return (this.rightX() > rect.x && rect.rightX() > this.x && this.topY() > rect.y && rect.topY() > this.y);
		}
	}
	
	//====================================================================== graphics section
	
		public static void displayGraphics(Packing packing) {
			JFrame frame = new JFrame();
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setSize(packing.container.width, packing.container.height);
			JPanel panel = new GraphicsPanel(packing);
			frame.add(panel);
			frame.setVisible(true);
		}
		
		private static class GraphicsPanel extends JPanel {
			
			Packing packing;
			
			private GraphicsPanel(Packing packing) {
				this.packing = packing;
			}
			
			@Override
			public void paintComponent(Graphics graphics) {
				super.paintComponent(graphics);
				Graphics2D g = (Graphics2D) graphics;
				
				g.setColor(Color.WHITE);
				g.fillRect(0, 0, getWidth(), getHeight());
				
				for (Rectangle rect : packing.rects) {
					g.setColor(Color.YELLOW);
					g.fillRect(rect.x, getHeight() - rect.y - rect.height, rect.width, rect.height);
					g.setColor(Color.BLACK);
					g.drawRect(rect.x, getHeight() - rect.y - rect.height, rect.width, rect.height);
				}
				
				g.setColor(Color.BLACK);
				g.drawLine(0, 1, getWidth(), 1);
			}
		}
}
